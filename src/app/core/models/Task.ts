import { StateHistory } from "./StateHistory";

export interface Task {
    id?: number; 
    title: string;
    description: string;
    dueDate: Date;
    completed: boolean;
    stateHistory: StateHistory[];
    notes : string[]
}