export interface StateHistory {
    state: string;
    date: Date;
}