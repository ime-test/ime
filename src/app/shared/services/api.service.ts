import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject, catchError, of, retry } from 'rxjs';




@Injectable({
  providedIn: 'root'
})
export class ApiService {


  apiUrl = 'http://localhost:3000/tasks';
  httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods':'*',
    'strict-origin-when-cross-origin':'*'
  })
};
private subject = new Subject<any>();
private showAddTask:boolean = false;

  constructor(private http: HttpClient) { }

  get(): Observable<any> {
    return this.http.get<any[]>(`${this.apiUrl}`).pipe(
      retry(3), catchError(this.handleError<any>('get')));
  }

  update(id: any, data: any): Observable<any> {
    return this.http.put<any>(`${this.apiUrl}/${id}`, data, this.httpOptions)
      .pipe(
        catchError(this.handleError('update', data))
      );
  }

  create(data: any): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}`, data, this.httpOptions)
      .pipe(
        catchError(this.handleError('create', data))
      );
  }

  delete(id: any): Observable<any> {
    return this.http.delete<any>(`${this.apiUrl}/${id}`, this.httpOptions)
      .pipe(
        catchError(this.handleError('delete'))
      );
  }


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      this.log(`${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }

  private log(message: string) {
    console.log(message);
  }

  onToggle():Observable<any>{
    return this.subject.asObservable();
  }

  toggleAddTask():void{
    this.showAddTask = !this.showAddTask;
    this.subject.next(this.showAddTask);
  }
  
}
