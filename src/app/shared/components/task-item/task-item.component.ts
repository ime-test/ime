import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Task } from '../../../core/models/Task';
import { faTimes } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-task-item',
  templateUrl: './task-item.component.html',
  styleUrls: ['./task-item.component.css']
})
export class TaskItemComponent implements OnInit {


  @Input() task: Task = 
  {
    completed : true,
    description : "",
    dueDate : new Date(),
    notes : [],
    stateHistory : [],
    title :""
  };
  @Output() onDeleteTask: EventEmitter<Task> = new EventEmitter()
  @Output() onToggle: EventEmitter<Task> = new EventEmitter();
  faTimes = faTimes;

  constructor() { }

  ngOnInit(): void {
  }

  onDelete(task: Task){
    this.onDeleteTask.emit(task);
  }

  onToggleTask(task: Task){
    this.onToggle.emit(task);
  }

}
