import { Component, OnInit } from '@angular/core';

import { ApiService } from '../../services/api.service';
import { Task } from '../../../core/models/Task';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {

  tasks: Task[] = [];
  

  constructor(
    private apiService: ApiService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.get();
  }

  get():void{
    this.apiService.get().subscribe({
      next: (resp) => {
        this.tasks = resp;
        this.toastr.success('Success')
      },
      error: (e) => {
        console.log(e);
      },
      complete: () => this.toastr.success('Success')
    });
  }

  delete(id: string): void {
    this.apiService.delete(id).subscribe({
      next: () => {
        this.get();
      },
      error: (e) => {
        console.log(e);
      },
      complete: () => console.info('complete')
    });
  }

  addTask(task: Task){
    this.apiService.create(task).subscribe({
      next: () => {
        this.get();
      },
      error: (e) => {
        console.log(e);
      },
      complete: () => console.info('complete')
    });
    // this.tasks.push(task);
  }

  deleteTask(task: Task){
    this.apiService.delete(task.id).subscribe({
      next: () => {
        this.get();
      },
      error: (e) => {
        console.log(e);
      },
      complete: () => console.info('complete')
    });
  }

  toggleTask(task: Task){
    this.apiService.update(task.id, task).subscribe({
      next: () => {
        this.get();
      },
      error: (e) => {
        console.log(e);
      },
      complete: () => console.info('complete')
    });
  }


}