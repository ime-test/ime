import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Task } from '../../../core/models/Task';
import { ApiService } from '../../services/api.service';


@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css'],
})
export class AddTaskComponent implements OnInit {
  @Output() onAddTask: EventEmitter<Task> = new EventEmitter();

  form: FormGroup;
  showAddTask: boolean = false;
  suscription: Subscription;

  constructor(private service: ApiService, private formBuilder: FormBuilder) {

    this.suscription = this.service
      .onToggle()
      .subscribe((value) => (this.showAddTask = value));
    this.form = this.formBuilder.group({
      title: ['', [Validators.required]],
      description: ['', [Validators.required]],
      dueDate: ['', [Validators.required]],
    });
    
  }

  ngOnInit(): void {}

  get Title() {
    return this.form.get('title');
  }
  get Description() {
    return this.form.get('description');
  }

  get DueDate() {
    return this.form.get('dueDate');
  }

  onSubmit(event: Event) {
    let submitData: Task
    event.preventDefault();
    if (this.form.valid) {
      submitData = {
        id: this.idGenerator(),
        title: this.form.value.title,
        description: this.form.value.description,
        dueDate: this.form.value.dueDate,
        completed: true,
        notes : [],
        stateHistory: []
      };        
      this.onAddTask.emit(submitData);
      this.form.reset();
      alert('Todo salio bien ¡Tarea Eviada!');
    } else {
      this.form.markAllAsTouched();
    }
  }

  idGenerator(): number {
    let date = new Date();
    date.toDateString();
    let id =
      String(date.getFullYear()) +
      String(date.getMonth() + 1).padStart(2, '0') +
      String(date.getUTCDate()).padStart(2, '0') +
      String(date.getUTCHours()).padStart(2, '0') +
      String(date.getUTCMinutes()).padStart(2, '0') +
      String(date.getUTCSeconds()).padStart(2, '0');
    return Number(id);
  }
}
