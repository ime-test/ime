import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  
  title:string = 'Agregar';
  showAddTask:boolean = false;
  suscription?: Subscription;

  constructor(
    private apiService: ApiService,
    private router: Router
  ) {
    this.suscription = this.apiService.onToggle()
                              .subscribe(value => this.showAddTask = value);
   }

  ngOnInit(): void {
  }

  toggleAddTask(){
    this.apiService.toggleAddTask();
  }

  hasRoute(route: string){
    return this.router.url === route;
  }

}
